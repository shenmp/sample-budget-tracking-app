import {Button} from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'

export default function ArchiveButton({transactionId}){
// console.log(transactionId)
	const archive = (transaction) => {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions/${transaction}`,{
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				Swal.fire(
				  'Done!',
				  'Archived succesfully',
				  'success'
				)
				Router.reload()
			} else {
				Router.push('/error')
			}
		})
	}
	return (
		<Button variant="danger" onClick={()=> archive(transactionId)}>Archive</Button>
		)
}