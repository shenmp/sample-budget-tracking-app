import {useContext} from 'react'
import {Navbar, Nav} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'
import styles from '../styles/Custom.module.css'

export default function NavBar(){
	const {user} = useContext(UserContext)
	// console.log(user.isAdmin)
	return(
	<Navbar variant="dark" className={styles.bg} expand="lg">
		<Navbar.Brand href="#">Spendy</Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse id="basic-navbar-nav">
		<Nav className="mr-auto">
			{

				(user.id !== null) ?
					
					<>
						<Link href="/transactions"><a className="nav-link">Transactions</a></Link>
						<Link href="/transactions/Chart"><a className="nav-link">Breakdown</a></Link>
						<Link href="/logout"><a className="nav-link">Logout</a></Link>
					</>
				:
				<>
					<Link href="/register"><a className="nav-link">Register</a></Link>
					<Link href="/login"><a className="nav-link">Login</a></Link>
				</>
			}

		</Nav>
	</Navbar.Collapse>
	</Navbar>
)
}