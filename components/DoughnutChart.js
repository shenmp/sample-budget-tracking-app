import {Doughnut} from 'react-chartjs-2'

export default function DoughnutChart ({category, amount}) {
	// console.log(category)
	// console.log(amount)

	return (
		<Doughnut
			data={{
				datasets:[{
					data: [category, amount],
					backgroundColor: ['red' , 'green']
				}],
				labels: ['Category', 'Amount']
			}}
			redraw={false} 
		/>
		
		)
}
