import {Button} from 'react-bootstrap'
import Router from 'next/router'


export default function UpdateButton({transactionId}) {
	// console.log(transactionId)
	const redirectToEditForm = (transaction) => {
		Router.push({
			pathname:'/transactions/edit',
			query:{transactionId: transaction}
		})
	}



	return (
		<Button variant="success" className="mr-1" onClick={()=> redirectToEditForm(transactionId)}> Update </Button>
		)
}
