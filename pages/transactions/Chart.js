import {useState, useContext, useEffect} from 'react'
import {Form, Button, Jumbotron} from 'react-bootstrap'
import DoughtnutChart from '../../components/DoughnutChart'
import UserContext from '../../UserContext'
import toNum from '../../helpers/toNum'
export default function Chart ({data}) {
	const { user } = useContext(UserContext);
	// console.log(user)
	// console.log(data)

	const transaction = data.filter(transaction => transaction.userId === user.id)
	const category = transaction.filter(transaction=> transaction.isActive === true).map(category => category.categoryName)
	const amountPerCategory = transaction.filter(transaction=> transaction.isActive === true).map(amount => amount.amount)
	// console.log(amountPerCategory)
	// set state variables

	const [categoryName, setCategoryName] = useState('')
	const [amount, setAmount]=  useState('')
	const [watcher, setWatcher] = useState(false)

	// for (let i=0 ; i< category.length; i++) {
	// 	let newCategory = category[i]
	// 	console.log(categoryName)
	// }



console.log('Actual Array: ' + category);

var filteredArr = category.filter(function(item, index) {
  if (category.indexOf(item) == index)
    return item;
});

console.log('Filtered Array: ' + filteredArr);

	useEffect(()=> {
		setCategoryName(categoryName)
		setAmount(amountPerCategory)
	}, [watcher])

	function Donut (e) {
		e.preventDefault()
			setWatcher(true)
		
	}

	return (
		<div>
		<Jumbotron>
		</Jumbotron>
		<DoughtnutChart category={category} amount={amountPerCategory} />
		</div>
		)
}


export async function getServerSideProps() {

	const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`)

	const data = await res.json()

	return {
		props : {
			data
		}
	}
}
