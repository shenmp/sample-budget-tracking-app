import {useContext, useState, useEffect} from 'react'
import {Jumbotron, Table, Button} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../../UserContext'
import UpdateTransaction from '../../components/UpdateTransaction'
import ArchiveButton from '../../components/ArchiveButton'
import styles from '../../styles/Custom.module.css'

export default function allTransact ({data}) {
	const { user } = useContext(UserContext);
	// console.log(user)
	// console.log(data)

	const transaction = data.filter(transaction => transaction.userId === user.id)


	if (user.id === null){
		return(
			<h1> log in first </h1>
			)
		
	} else if (user.id && transaction.length > 0) {
	return (
		<div>
			<h1 className="text-center p-3 mt-2">Records History</h1>
			<Table responsive="sm" striped bordered hover variant="dark" >
			  <thead>
			    <tr>
			      <th>Type</th>
			      <th>Category</th>
			      <th>Amount</th>
			      <th>Description</th>
			      <th>Date</th>
			      <th>Status</th>
			      <th>Actions</th>
			    </tr>
			  </thead>
			  <tbody>
			  	{transaction.map(transactions =>{
        		return(
        			<tr key={transactions._id}>
        				<td>{transactions.type}</td>
        				<td>{transactions.categoryName}</td>
        				<td>{transactions.amount}</td>
        				<td>{transactions.description}</td>
        				<td>{transactions.dateAdded}</td>
		        		<td>{transactions.isActive === true ? 'Active' : 'Inactive'}</td>
		        		<td>
		        			<UpdateTransaction transactionId={transactions._id}/>
                            <ArchiveButton transactionId={transactions._id}/>
		        		</td>
		        	</tr>
        	)
        	})}
			  </tbody>
			</Table>
			
			</div>
		)
	} else {
		return (
			<div>
			<h1 className="text-center p-5">Transactions</h1>
			<Table striped bordered hover variant="dark">
			  <thead>
			    <tr>
			      <th>Type</th>
			      <th>Amount</th>
			      <th>Description</th>
			      <th>Date</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr><td colSpan="4"><i>No transactions found</i></td></tr>
			  </tbody>
			</Table>
			</div>)
		
	}

		
}

export async function getServerSideProps() {

	const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`)

	const data = await res.json()

	return {
		props : {
			data
		}
	}
}

