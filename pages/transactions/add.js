import {useState, useEffect, useContext} from 'react'
import {Container, Card, Form, Button, Alert, Jumbotron} from 'react-bootstrap'
import UserContext from '../../UserContext'
import Router from 'next/router'
import styles from '../../styles/Custom.module.css'

export default function create() {
	const{user} = useContext(UserContext)

	// declare form input states
	const [type, setType] = useState('')
	const [category, setCategory] = useState('')
	const [description, setDescription] = useState('')
	const [amount, setAmount] = useState('')

	const [isDisabled, setIsDisabled] = useState(true)
	const [notify, setNotify] = useState(null)

	const [show, setShow] = useState(true)

	useEffect(() => {
		if(user.id === '') {
			Router.push('/login')
		}
	}, [])

	// data validation : check if fields are empty or not
	useEffect(() =>{
		if(type !== '' && category !== '' && description !== '' && amount !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [type, category, description, amount])

	// function to create user

	function addTransaction(e) {
		e.preventDefault()

		console.log( user, {type: type,
				categoryName: category,
				description:description,
				amount:amount})
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions/` ,{
			method:'POST',
			headers : {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`,
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify ({
				type: type,
				categoryName: category,
				description:description,
				amount:amount,
				userId: user.id
			})
		})
		.then(res => res.json())
		.then((data) => {
			if (data) {
				setType('')
				setCategory('')
				setDescription('')
				setAmount('')
				setNotify(true)
			} else {
				setNotify(false)
			}
		})
	}

	if(user.id) {
		return (
		<div>
   <Container>
      <Card className={styles.shadow}>
        <Card.Body>
          <Card.Title>Add Transaction</Card.Title>
          <Form onSubmit={e=> addTransaction(e)}>
            <Form.Group controlId="type">
              <Form.Label>Income or Expense:</Form.Label>
              <Form.Control
                onChange={(e) => {
                  setType(e.target.value);
                }}
                value={type}
                as="select"
                custom
              >
                <option>Income</option>
                <option>Expense</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="category">
              <Form.Label>Enter Category:</Form.Label>
              <Form.Control
                onChange={(e) => setCategory(e.target.value)}
                value={category}
                as="select"
                custom
              >
                <option>Food</option>
                <option>Utility</option>
                <option>Transportaion</option>
                <option>Allowance</option>
                <option>Rent</option>
                <option>Salary</option>
                <option>Cash</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>Enter description:</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="amount">
              <Form.Label>Amount:</Form.Label>
              <Form.Control
                type="number"
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
                required
              />
            </Form.Group>

         <Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
          </Form>
        </Card.Body>
      </Card>
    </Container>
			


		{
			(notify ==null) ? 
				null 
			:
				(notify===true) ?
				<Alert variant="success" className="m-1" show={show} onClose={() => setShow(false)} dismissible >Transaction added!</Alert>
				:
				<Alert variant="danger" className="m-1" show={show} onClose={() => setShow(false)} dismissible >Failed to create a course</Alert>
		}
		
		</div>
		)
	} else {
		return (
				<Jumbotron className={`${styles.jumbo}`}>
					<h1 className="text-center p-5"> 404 | Not Found</h1>
				</Jumbotron>)
	}
	
}