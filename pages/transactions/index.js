import {useContext, useState, useEffect} from 'react'
import {Jumbotron, Table, Button} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../../UserContext'
import UpdateTransaction from '../../components/UpdateTransaction'
import ArchiveButton from '../../components/ArchiveButton'
import styles from '../../styles/Custom.module.css'

export default function index ({data}) {
	const { user } = useContext(UserContext);
	// console.log(user)
	// console.log(data)
	const [totalIncome, setTotalIncome] = useState('')
	const [totalExpenses, setTotalExpenses] = useState('')
	const [totalBalance, setTotalBalance] = useState('')
	const [watcher, setWatcher] = useState(false)
	const [watcher2, setWatcher2] = useState(false)
	const transaction = data.filter(transaction => transaction.userId === user.id)
	const filteredTransaction = transaction.filter(transaction=> transaction.isActive === true)
	console.log(filteredTransaction)
	
	let income = filteredTransaction.map(transactions => transactions.type==="Income")
	let expenses = filteredTransaction.map(transactions => transactions.type==="Expense")

	useEffect(()=> {
		if(income) {
			fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${user.id}`)
		.then(res => res.json())
		.then(data => {
			console.log(data.totalIncome)
			setTotalIncome(data.totalIncome)
		})
		} else {
			return null
		}
	}, [watcher])

	useEffect(()=> {
		if (expenses) {
			fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${user.id}`)
		.then(res => res.json())
		.then(data => {
			console.log(data.totalIncome)
			setTotalExpenses(data.totalExpenses)
		})
		} else {
			return null
		}
	}, [watcher2])

	function refresh (e) {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions/sumIncome/${user.id}`)
		.then(res => res.text())
		.then(data => {
			setWatcher(true)
		})
	}
	function refresh2 (e) {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions/sumExpenses/${user.id}`)
		.then(res => res.text())
		.then(data => {
			setWatcher2(true)
		})
	}

	function getCurrentBalance (e) {
		let balance = totalIncome - totalExpenses
		setTotalBalance(balance)
		// console.log(balance)
	}
	if (user.id === null){
		return(
			<h1> log in first </h1>
			)
		
	} else if (user.id && filteredTransaction.length > 0) {
	return (
		<div>
			<h1 className="text-center p-3 mt-2">Transactions</h1>
			<Table responsive="sm" striped bordered hover variant="dark" >
			  <thead>
			    <tr>
			      <th>Type</th>
			      <th>Category</th>
			      <th>Amount</th>
			      <th>Description</th>
			      <th>Date</th>
			      <th>Actions</th>
			    </tr>
			  </thead>
			  <tbody>
			  	{filteredTransaction.map(transactions =>{
        		return(
        			<tr key={transactions._id}>
        				<td>{transactions.type}</td>
        				<td>{transactions.categoryName}</td>
        				<td>{transactions.amount}</td>
        				<td>{transactions.description}</td>
        				<td>{transactions.dateAdded}</td>
		        		<td>
		        			<UpdateTransaction transactionId={transactions._id}/>
                            <ArchiveButton transactionId={transactions._id}/>
		        		</td>
		        	</tr>
        	)
        	})}
			  </tbody>
			</Table>
			<Jumbotron className={`${styles.jumbo}`}>
				<h1><b>Total Income :</b> {totalIncome}</h1>
				<h1><b>Total Expenses : </b>{totalExpenses}</h1>
				<h1><b>Current Balance :</b> {totalBalance}</h1>
			</Jumbotron>
			<Button className ={`${styles.wavebtn} m-2`}> <Link href="/transactions/add">Add Transaction</Link> </Button>
			<Button className ={`${styles.wavebtn} m-2`}> <Link href="/transactions/allTransact">Records History</Link> </Button>
			<Button className ={`${styles.wavebtn} m-2`}onClick={e => refresh(e)}>Total Income</Button>
			<Button className ={`${styles.wavebtn} m-2`} onClick={e => refresh2(e)}>Total Expenses</Button>
			<Button className ={`${styles.wavebtn} m-2`} onClick={e => getCurrentBalance(e)}>Total Balance</Button>
			
			</div>
		)
	} else {
		return (
			<div>
			<h1 className="text-center p-5">Transactions</h1>
			<Table striped bordered hover variant="dark">
			  <thead>
			    <tr>
			      <th>Type</th>
			      <th>Amount</th>
			      <th>Description</th>
			      <th>Date</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr><td colSpan="4"><i>No transactions found</i></td></tr>
			  </tbody>
			</Table>
			<Button className ={`${styles.wavebtn} m-2`}  ><Link href="/transactions/add">Add Transaction</Link> </Button>
			<Button className ={`${styles.wavebtn} m-2`}> <Link href="/transactions/allTransact">Records History</Link> </Button>
			</div>)
		
	}

		
}

export async function getServerSideProps() {

	const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`)

	const data = await res.json()

	return {
		props : {
			data
		}
	}
}

