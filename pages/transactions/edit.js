import {useState, useEffect, useContext} from 'react'
import {Form, Button, Alert, Card, Container} from 'react-bootstrap'
import Router from 'next/router'
import {useRouter} from 'next/router' // useRouter is a next router hook that will hook the data from the Router.push() objects
import UserContext from '../../UserContext'
import styles from '../../styles/Custom.module.css'

export default function edit() {
	const router = useRouter()
	const {transactionId} = router.query
	const { user } = useContext(UserContext);
	console.log(transactionId)
	// console.log(user.id)

	// declare state variables
	const [type, setType] = useState('')
	const [category, setCategory] = useState('')
	const [amount, setAmount] = useState('')
	const [description, setDescription] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	const [notify, setNotify] = useState(false)
	const [watcher, setWatcher] = useState(false)
 

	useEffect(()=> {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions/${transactionId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setType(data.type)
			setCategory(data.categoryName)
			setDescription(data.description)
			setAmount(data.amount)
			setWatcher(false)
			
		})
	}, [watcher])

	useEffect(()=> {
		if (type !== '' && category !== '' && description !== '' && amount !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [type, category, description, amount])

	function editTransaction(e) {
		// create a fetch request that will update a course, if successful, redirect to Admin Dashboard	else, return an alert that will notify 'Failed to update a course'

		e.preventDefault()
		    console.log(user, {
  			type: type,
					categoryName: category,
					description:description,
					amount:amount,
					userId: `${user.id}`
   			 });

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions/${transactionId}`,{ 
			method:'PUT',
			headers :{
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
				}, 
				body : JSON.stringify({
					type: type,
					categoryName: category,
					description:description,
					amount:amount,
					userId: `${user.id}`
				})
			})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Router.push('/transactions')
				setWatcher(true)
			} else {
				setNotify(true)
			}
		})
	}

	if (user.id !== null) {
			return ( 
		<div>
	<Container>
      <Card className={`${styles.shadow} col-lg- 12 col-md-12 col-sm-12 col-xs-12`}>
        <Card.Body>
          <Card.Title className="text-center p-4">Update Transaction</Card.Title>
          <Form onSubmit={e=> editTransaction(e)}>
            <Form.Group controlId="type">
              <Form.Label>Income or Expense:</Form.Label>
              <Form.Control
                onChange={(e) => {
                  setType(e.target.value);
                }}
                value={type}
                as="select"
                custom
              >
                <option>Income</option>
                <option>Expense</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="category">
              <Form.Label>Category:</Form.Label>
              <Form.Control
                onChange={(e) => setCategory(e.target.value)}
                value={category}
                as="select"
                custom
              >
              	<option>Salary</option>
                <option>Food</option>
                <option>Allowance</option>
                <option>Rent</option>
                <option>Utility</option>
                <option>Cash</option>
                <option>Transportaion</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>Enter Description:</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="amount">
              <Form.Label>Enter Amount:</Form.Label>
              <Form.Control
                type="number"
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
                required
              />
            </Form.Group>

         <Button className={styles.wavebtn} type="submit" disabled={isDisabled}>Submit</Button>
          </Form>
        </Card.Body>
      </Card>
    </Container>

		{
		(notify === true )? 

				<Alert variant="danger" show={show} onClose={() => setShow(false)} dismissible >Failed to update</Alert>
		:null
		}
		</div>
		)
	} else {
		return <h1 className="text-center m-5"> Log in first! </h1>
	}
}