import {useState,useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import {Container} from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Head from 'next/head'
import {UserProvider} from '../UserContext'

function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({
		id:null
	})
	// function for clearing localstorage and user, for logout also
	const unSetUser = () => {
		localStorage.clear()
		setUser({
			id:null
		})
	}
	useEffect(()=> {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
			headers : {
				// the user info that will be retrieved from the API depends on the value of the encoded token in the localStorage
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data._id) {
				setUser({
					id:data._id
				})
			} else {
				// else, if the token is not valid or there is no authenticated user, keep the user properties to null value
				setUser({
					id:null
				})
			}
		})
	}, [user.id])


 return (
		<div>
		<Head>
			<title>Spendy</title>
			<meta name="viewport" content="initial-scale=1.0, width=device-width"/>
		</Head>
		<UserProvider value={{user,setUser, unSetUser}}>
			<NavBar />
			<Container>
			<Component {...pageProps} />
			</Container>
		</UserProvider>
		</div>
	 )
  
}

export default MyApp
