import {useState, useEffect, useContext} from 'react'
import {Row, Col, Form, Button, Container} from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import { GoogleLogin } from 'react-google-login'
import Swal from 'sweetalert2'
import styles from '../styles/Custom.module.css'
import error from './error'

export default function login() {
	// use the UserContext and desctructure it to obtain the setUser state setter from our app entry point
	const { setUser } = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(()=> {
		if(email !== '' && password !== '') {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email,password])

	function authenticate(e) {
		e.preventDefault()
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
			method: 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify ({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data.accessToken) {
				// store AccessToken in local storage
				localStorage.setItem('token', data.accessToken)
				// console.log(data.accessToken)
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details` , {
					headers:{
						Authorization : `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					setUser({
						id:data._id
					})
					console.log('success')
					Router.push('/transactions')
				})

			} else {
				Router.push('/error')
				console.log('auth failed')
			}
		})
	}
	// will capture Google login response from onSuccess/onFailure property below
	const captureLoginResponse = (response) =>{
		console.log(response);
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/verify-google-id-token` , {
			method:'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({tokenId:response.tokenId})
		})
		.then( res => res.json())
		.then(data => {
			// console.log(data)
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken)
				setUser({id:data._id})
				Router.push('/transactions')
				// console.log('success may token')
			} else {
				if (data.error === 'login-type-error') {
					Swal.fire({
					  title: 'Login Type Error!',
					  text: 'You may have registered through a different login procedure',
					  icon: 'error',
					  confirmButtonText: 'Close'
					})
				} else {
					Swal.fire({
					  title: 'Google auth error!',
					  text: 'Google Authentication procedure failed',
					  icon: 'error',
					  confirmButtonText: 'Close' })
				}
			}
		})
	}

	return(
		<div>
		<Container> 
			<div className={styles.grid}>
			<div className={styles.landing}>
				<img
		    		  className="d-block w-100"
		      		src="/landing.png"
		      		alt="Landing image" />
		    	</div>
				<Form onSubmit={e => authenticate(e)} className={styles.loginform}>
					<Form.Group>
						<Form.Label> Email : </Form.Label>
						<Form.Control
							type="email"
							onChange={e => setEmail(e.target.value)}
							value={email}
						/>
					</Form.Group>			
					<Form.Group>
						<Form.Label> Password : </Form.Label>
						<Form.Control
							type="password"
							onChange={e => setPassword(e.target.value)}
							value={password}
						/>
					</Form.Group>
					<div className={styles.buttons}>
					<Button className={styles.loginbtn} type="submit" disabled={isDisabled}> Log in</Button>
					<GoogleLogin clientId='1045290598125-skt9kqe86qn5cnl614se0fq3cqkgt1ql.apps.googleusercontent.com' onSuccess={captureLoginResponse} onFailure={captureLoginResponse} cookiePolicy={'single_host_origin'}/>
					</div>
				</Form>
				</div>
				</Container> 
			</div>
		)
}

