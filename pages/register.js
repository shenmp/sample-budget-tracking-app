import {useState, useEffect} from 'react'
import {Row, Col, Form, Button, Container, Card} from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import styles from '../styles/Custom.module.css'

export default function register() {
	// form input state hooks
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [verifyPassword, setVerifyPassword] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')

	// state to determine the status of form submit button
	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(()=> {
		// validate the password: check if the passwords are not empty and are equal value
		if((password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [password, verifyPassword])

	//function for submitting user registration form
	function registerUser(e)  {
		e.preventDefault()
		// alert('You submitted!')
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName, //state variable yung pangalawa
				lastName: lastName,
				email:email,
				password:password
			})
		})
		.then(res => res.json())
		.then(data => {
			// if registration is successful, redirect to login
			if (data.error) {
				if (data.error == 'use-google-login') {
					Swal.fire(
						'Google Account Registered',
						'You may have registered through a different procedure',
						'error'
						)
				} else if (data.error === 'account-already-registered') {
					Swal.fire(
						'Already registered',
						'Your email is already registered on our app',
						'error'
						)
				}
			} else {
				Router.push('/login')
			}
		})
	}

	return(
		<div>
	<Container>
      <Card className={`${styles.shadow} col-lg- 12 col-md-12 col-sm-12 col-xs-12`}>
        <Card.Body>
          <Card.Title className="text-center p-2">Register</Card.Title>
          	<Form onSubmit={ (e) => registerUser(e)} className="mt-4">
			<Form.Group>
				<Form.Label>First name</Form.Label> 
				<Form.Control 
					type="text"
					onChange={e => setFirstName(e.target.value)}
					value={firstName} 
					required>
				</Form.Control>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last name</Form.Label> 
				<Form.Control 
					type="text" 
					onChange={e => setLastName(e.target.value)}
					value={lastName}
					required>
				</Form.Control>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email</Form.Label> 
				<Form.Control 
					type="text"
					onChange={e => setEmail(e.target.value)}
					value={email} 
					required>
				</Form.Control>
			</Form.Group>			
			<Form.Group>
				<Form.Label>Password</Form.Label> 
				<Form.Control 
					type="password" 
					onChange={e => setPassword(e.target.value)}
					value={password} 
					required>
				</Form.Control>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password</Form.Label> 
				<Form.Control 
					type="password" 
					onChange={e => setVerifyPassword(e.target.value)}
					value={verifyPassword}
					required>
				</Form.Control>
			</Form.Group>
			<Button variant="success" type="submit" disabled={isDisabled}> Register</Button>
		</Form>
        </Card.Body>
      </Card>
    </Container>
    </div>


		
		)
}