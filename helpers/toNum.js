export default function toNum(string) {

	const strArr = [...string]
	// get elements that are not equal to a comma
	const filteredArr = strArr.filter(element => element !== ",")
	return parseInt(filteredArr.reduce((x,y)=> x+y))
	// console.log(filteredArr) ["1", "0","0","0"]
	// console.log(filteredArr.reduce((x,y)=> x+y)) returns 2000

	// console.log(filteredArr.reduce((total,num)=> return))
	// console.log(numbers.reduce((total,num) => total+num))
}

/*
	convert a string into array form = Juan Dela Cruz => ['J', 'u', 'a', 'n'...]
	filter out the commas/spaces on the array of string
	reduce the filtered array into a string w/o commas
	Return the string into a parse integer

*/