import React from 'react'

// create a context object for state management
const UserContext = React.createContext()


// Context provider that allows for consumming components to subscribe to context changes
export const UserProvider = UserContext.Provider

export default UserContext 